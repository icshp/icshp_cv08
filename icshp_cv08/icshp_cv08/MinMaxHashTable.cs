﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;

namespace icshp_cv08
{
    /// <summary>
    /// Prvek Linked listu
    /// Původně bylo s KeyValuePair, ale bylo to pomalejší.. 0,2s
    /// </summary>
    /// <typeparam name="TKey">Klíč</typeparam>
    /// <typeparam name="TValue">Hodnota</typeparam>
    class Element<TKey, TValue>
    {
        public TKey Key;
        public TValue Value;
        public Element<TKey, TValue> Next;

        public Element(TKey key, TValue value)
        {
            Key = key;
            Value = value;
            Next = null;
        }
    }

    /// <summary>
    /// Datová struktura - HashTable
    /// </summary>
    /// <typeparam name="TKey">Klíč</typeparam>
    /// <typeparam name="TValue">Hodnota</typeparam>
    public class MinMaxHashTable<TKey, TValue>
    {
        private readonly int _size;
        private readonly Element<TKey, TValue>[] _elementArray;

        public int Count { get; private set; }
        public TKey Minimum { get; private set; }
        public TKey Maximum { get; private set; }
        public IEnumerable<KeyValuePair<TKey, TValue>> this[TKey minimum, TKey maximum]
        {
            get
            {
                List<KeyValuePair<TKey, TValue>> list = new List<KeyValuePair<TKey, TValue>>();
                for (int i = 0; i < _size; i++)
                {
                    Element<TKey, TValue> rootElement = _elementArray[i];
                    while (rootElement != null)
                    {
                        if (ApprovedToInsert(rootElement.Key, minimum, maximum))
                            list.Add(new KeyValuePair<TKey, TValue>(rootElement.Key, rootElement.Value));
                        rootElement = rootElement.Next;
                    }
                }
                return list;
            }
        }

        /// <summary>
        /// Konstruktor s volbou velikosti pole
        /// </summary>
        /// <param name="Size"></param>
        public MinMaxHashTable(int capacity)
        {
            Count = 0;
            _size = capacity;
            _elementArray = new Element<TKey, TValue>[capacity];
             Minimum = default(TKey);
             Maximum = default(TKey);
        }

        /// <summary>
        /// Konstruktor bez volby velikosti pole
        /// </summary>
        public MinMaxHashTable() : this(20) { }

        private int GetElementIndex(TKey tKey)
        {
            int elementIndex = tKey.GetHashCode() % _size;
            if (elementIndex < 0)
            {
                return Math.Abs(elementIndex);
            }
            return elementIndex;
        }

        /// <summary>
        /// Vložení páru do HashTable
        /// </summary>
        /// <param name="tKey">Klíč</param>
        /// <param name="tValue">Hodnota</param>
        /// <exception cref="ArgumentException">Klíč existuje</exception>
        public void Add(TKey tKey, TValue tValue)
        {
            int elementIndex = GetElementIndex(tKey);

            Element<TKey, TValue> rootElement = _elementArray[elementIndex];
            while (rootElement != null)
            {
                if (rootElement.Key.Equals(tKey))
                {
                    throw new ArgumentException();
                }
                rootElement = rootElement.Next;
            }

            rootElement = _elementArray[elementIndex];
            Element<TKey, TValue> element = new Element<TKey, TValue>(tKey, tValue);
            element.Next = rootElement;
            _elementArray[elementIndex] = element;
            Count++;
            MinMaxKey(tKey);
        }

        /// <summary>
        /// Metoda Contains - Je v kolekci?
        /// </summary>
        /// <param name="tKey">Klíč</param>
        /// <returns>Je/Není</returns>
        public bool Contains(TKey tKey)
        {
            try
            {
                Get(tKey);
                return true;
            }
            catch (Exception) //Optimalizace proti duplicitě
            {
                return false;
            }
        }

        /// <summary>
        /// Get - Získá z kolekce, nebo vyhodí vyjímku.
        /// </summary>
        /// <param name="tKey">Klíč</param>
        /// <exception cref="KeyNotFoundException">Klíč nebyl nalezen</exception>
        /// <returns>Hodnota</returns>
        public TValue Get(TKey tKey)
        {
            Element<TKey, TValue> rootElement = _elementArray[GetElementIndex(tKey)]; 

            while (rootElement != null)
            {
                if (rootElement.Key.Equals(tKey))
                {
                    return rootElement.Value;
                }
                rootElement = rootElement.Next;
            }

            throw new KeyNotFoundException();
        }

        /// <summary>
        /// Metoda Remove - Odebrání z kolekce
        /// </summary>
        /// <exception cref="KeyNotFoundException">Klíč nenalezen</exception>
        /// <param name="tKey">Klíč</param>
        /// <returns>Hodnota</returns>
        public TValue Remove(TKey tKey)
        {
            int index = GetElementIndex(tKey);
            Element<TKey, TValue> element = _elementArray[index];
            Element<TKey, TValue> previousElement = null;

            while (element != null)
            {
                if (element.Key.Equals(tKey))
                    break;

                previousElement = element;
                element = element.Next;
            }

            //Nenašel se
            if (element == null)
                throw new KeyNotFoundException();

            //Je hlavním elementem
            if (previousElement == null)
            {
                _elementArray[index] = element.Next;
            }
            else //Má předcůdce
            {
                previousElement.Next = element.Next;
            }

            Count--;
            return element.Value;
        }

        /// <summary>
        /// Range
        /// </summary>
        /// <param name="minimum"></param>
        /// <param name="maximum"></param>
        /// <returns>IEnumerable</returns>
        public IEnumerable<KeyValuePair<TKey, TValue>> Range(TKey minimum, TKey maximum)
        {
            return this[minimum, maximum];
        }

        /// <summary>
        /// Pomocná metoda pro vkládání Min/Max
        /// </summary>
        /// <param name="key"></param>
        private void MinMaxKey(TKey key)
        {

            if (key.GetHashCode() < Minimum.GetHashCode())
            {
                Minimum = key;
            }
            if (key.GetHashCode() > Maximum.GetHashCode())
            {
                Maximum = key;
            }
        }

        /// <summary>
        /// Metoda pro porovnání klíčů
        /// </summary>
        /// <param name="tKey"></param>
        /// <param name="tKey2"></param>
        /// <returns>0,1,-1</returns>
        private int CompareTo(TKey tKey, TKey tKey2)
        {
            return tKey.GetHashCode().CompareTo(tKey2.GetHashCode());
        }

        /// <summary>
        /// Pomocná metoda pro vkládání do kolekce (list)
        /// Pokud je v určitém rozsahu, tak vrátí true;
        /// </summary>
        /// <param name="tKey"></param>
        /// <param name="minKey"></param>
        /// <param name="maxKey"></param>
        /// <returns></returns>
        private bool ApprovedToInsert(TKey tKey, TKey minKey, TKey maxKey)
        {
            
            return (CompareTo(tKey, minKey) >= 0) && (CompareTo(tKey, maxKey) <= 0);
        }

        /// <summary>
        /// Třízení kolekce pomocí Linq
        /// </summary>
        /// <param name="minimum"></param>
        /// <param name="maximum"></param>
        /// <returns>Vytřízená kolekce</returns>
        public IEnumerable<KeyValuePair<TKey, TValue>> SortedRange(TKey minimum, TKey maximum)
        {
            return this[minimum, maximum].ToList().OrderBy(x => x.Key);
        }
    }

}
